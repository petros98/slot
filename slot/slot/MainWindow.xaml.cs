﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace slot
{
    public partial class MainWindow : Window
    {
        Random rd = new Random();
        User user = new User("User1");
        int bet , step=5;
        public MainWindow()
        {            
            InitializeComponent();
        }
               
        private void Start_Click(object sender, RoutedEventArgs e)
        {
            if (step == 0 )
            {
                if (user.balance <= 0)
                {
                    MessageBox.Show("you lost");
                    App.Current.Shutdown();
                }
                else
                {
                    MessageBox.Show("you win \n" + " your balance : "  + user.balance);
                    App.Current.Shutdown();
                }
            }
            step--;
            userName.Content = user.name;
            int a, b, c;
            a = rd.Next(1, 9);
            b = rd.Next(1, 9);
            c = rd.Next(1, 9);
            block1.Content = a;
            block2.Content = b;
            block3.Content = c;
            if (a==7 && a==b && a==c)
            {
                user.balance += bet * 5;
                balance.Content = "Balance  " + user.balance;
            }
            else if (a==b && a==c && b==c && a!=7)
            {
                user.balance += bet * 4;
                balance.Content = "Balance  " + user.balance;
            }
            else if (a == b || b == c )
            {
                user.balance += bet * 2;
                balance.Content = "Balance  " + user.balance;
            }
            else
            {
                user.balance -= bet;
                balance.Content = "Balance  " + user.balance;
            }
            if (user.balance<=0)
            {
                MessageBox.Show("you lost");
            }
            
        }

        private void _bet15_Checked(object sender, RoutedEventArgs e)
        {
            bet = 15;
        }

        private void _bet5_Checked(object sender, RoutedEventArgs e)
        {
            bet = 5;
        }

        private void _bet10_Checked(object sender, RoutedEventArgs e)
        {
            bet=10;
        }
    }
}
